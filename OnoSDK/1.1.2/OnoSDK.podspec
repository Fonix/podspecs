#
# Be sure to run `pod lib lint OnoSDK.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'OnoSDK'
  s.version          = '1.1.2'
  s.summary          = 'A short description of OnoSDK.'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = <<-DESC
TODO: Add long description of the pod here.
                       DESC

  s.homepage         = 'https://www.yoco.co.za'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'Jacques Questiaux' => 'jacques@yoco.com' }
  s.source           = { :git => 'https://oauth2:2JkmjigexMvxjC9buodi@gitlab.com/yoco/ono-sdk-ios.git', :tag => s.version.to_s } #TODO: Change this source url to be public at some point, this expires on 31st dec 2020
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.ios.deployment_target = '8.0'

  s.source_files = 'OnoSDK/Classes/**/*'
  s.public_header_files = 'OnoSDK/Classes/**/*.h'

  # s.resource_bundles = {
  #   'OnoSDK' => ['OnoSDK/Assets/*.png']
  # }

  # s.public_header_files = 'Pod/Classes/**/*.h'
  
  s.dependency 'Analytics', '~> 3.0'
  s.dependency 'Gloss', '~> 3.1'
  
  s.dependency 'CocoaLumberjack/Swift' # Please remove once YocoSDK files are removed, or if all things relying on DDLog.h are removed
  s.dependency 'Reachability' # Please remove once YocoSDK files are removed, or if all things relying on Reachability are removed
  
  s.frameworks = 'Ono', 'ExternalAccessory'
  s.vendored_frameworks = 'OnoSDK/Libraries/Ono.framework'
  s.vendored_libraries = 'OnoSDK/Libraries/libqpos-ios-sdk.a', 'OnoSDK/Libraries/libMiuraSdk.a'
  
  
end
