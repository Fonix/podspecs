
#
# Be sure to run 'pod lib lint YocoSDK.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'YocoSDK'
  s.version          = '4.6.5-lldbfix'
  s.summary          = 'Yoco\'s payment sdk with UI interface'
  
  s.description      = <<-DESC
  The Yoco SDK will allow your app to pair with Yoco card machines and accept payments. It provides a built-in user interface to take the user through the payment flow.
  DESC
  
  s.homepage         = 'https://gitlab.com/yoco-public/yoco-sdk-mobile-ios'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'Jacques Questiaux' => 'support@yoco.com' }
  s.source           = { :git => 'https://gitlab.com/yoco-public/yoco-sdk-mobile-ios.git', :tag => s.version }
  s.social_media_url = 'https://www.facebook.com/YocoZA/'
  
  s.ios.deployment_target = '11.0'
  s.swift_version = '5.2.1'

  s.pod_target_xcconfig = { 'VALID_ARCHS' => 'arm64 x86_64' }
  
  # Ono.xcframework built from KN doesn't support this, so exclude it
  s.user_target_xcconfig = { 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64' }
  s.pod_target_xcconfig = { 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64' }

  s.vendored_frameworks = 'YocoSDK/' + s.name + '.xcframework', 'OnoSDK/OnoSDK.xcframework', 'OnoSDK/Ono.xcframework', 'YocoSDK/Libraries/YCLottie.xcframework', 'YocoSDK/Libraries/YCSwiftSignatureView.xcframework', 'YocoSDK/Libraries/YCReSwift.xcframework', 'YocoSDK/Libraries/YCGloss.xcframework'
  
  # OnoSDK dependancies
  s.frameworks = 'ExternalAccessory'
  
end
