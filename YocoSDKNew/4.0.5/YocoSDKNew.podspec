#
# Be sure to run `pod lib lint YocoSDK.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'YocoSDKNew'
  s.version          = '4.0.5'
  s.summary          = 'Yoco\'s payment sdk with UI interface'

  s.description      = <<-DESC
  The Yoco SDK will allow your app to pair with Yoco card machines and accept payments. It provides a built-in user interface to take the user through the payment flow.
                       DESC

  s.homepage         = 'https://www.yoco.co.za'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'Jacques Questiaux' => 'jacques@yoco.com' }
  s.source           = { :git => 'https://oauth2:2JkmjigexMvxjC9buodi@gitlab.com/yoco/yoco-payment-sdk-ios-ui.git', :tag => s.version.to_s }
  s.social_media_url = 'https://www.facebook.com/YocoZA/'

  s.ios.deployment_target = '9.0'

  s.source_files = s.name + '/Classes/**/*'
  s.swift_version = "5.0.1"
  
  s.resource_bundles = {
    s.name => [s.name + '/Assets/**/*']
  }
  s.resources = [s.name + '/Assets/*.{xcassets}']
  
  s.dependency 'ReSwift', '~> 5.0'
  s.dependency 'lottie-ios', '~> 3.1'
  s.dependency 'OnoSDK', '~> 1.0.0'
  s.dependency 'SwiftSignatureView', '~> 2.2' # requires deployment target 8.3
  s.dependency 'Gloss', '~> 3.1'
  s.dependency 'Alamofire', '~> 4.9'
  
#  s.pod_target_xcconfig = {
#    'SWIFT_ACTIVE_COMPILATION_CONDITIONS[config=Test]' => 'TEST'
#  }
  
end
