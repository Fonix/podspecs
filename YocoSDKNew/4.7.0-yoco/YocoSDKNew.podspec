#
# Be sure to run `pod lib lint YocoSDK.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'YocoSDKNew'
  s.version          = '4.7.0-yoco'
  s.summary          = 'Yoco\'s payment sdk with UI interface'

  s.description      = <<-DESC
  The Yoco SDK will allow your app to pair with Yoco card machines and accept payments. It provides a built-in user interface to take the user through the payment flow.
                       DESC

  s.homepage         = 'https://gitlab.com/yoco-public/yoco-sdk-mobile-ios'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'Jacques Questiaux' => 'jacques@yoco.com' }
  s.source           = { :git => 'https://oauth2:TYt5QjMa35Rox7_YA-q_@gitlab.com/yoco/yoco-payment-sdk-ios-ui.git', :tag => s.version.to_s } # Oauth token is read only, expires 31 jan 2026
  
  s.social_media_url = 'https://www.facebook.com/YocoZA/'

  s.ios.deployment_target = '9.0'

  s.source_files = s.name + '/Classes/**/*'
  s.swift_version = "5.2.1"
  
  s.pod_target_xcconfig = { 'VALID_ARCHS' => 'arm64 armv7 x86_64' }
  
  # Ono.xcframework built from KN doesn't support this, so exclude it
  s.user_target_xcconfig = { 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64' }
  s.pod_target_xcconfig = { 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64' }
  
# s.ios.resource_bundles = {
#    s.name => [s.name + '/Assets/**/*.{xib,storyboard,strings,json,otf}']
#  }
#  s.resources = [s.name + '/Assets/*.{xcassets}']

  s.resources = [s.name + '/Assets/**/*.{xib,storyboard,strings,json,otf,xcassets}']

  s.dependency 'OnoSDK', '1.7.0'
  
  s.vendored_frameworks = 'YocoSDKNew/Libraries/YCLottie.xcframework', 'YocoSDKNew/Libraries/YCSwiftSignatureView.xcframework', 'YocoSDKNew/Libraries/YCReSwift.xcframework', 'YocoSDKNew/Libraries/YCGloss.xcframework'
  
end
